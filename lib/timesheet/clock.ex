defmodule Timesheet.Clock do
  @callback now :: :calendar.datetime

  def now do
    Calendar.DateTime.from_erl!(clock.now, "Europe/Brussels", 0)
  end

  defp clock do
    Application.get_env(:timesheet, :clock)
  end
end
