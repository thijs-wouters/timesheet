defmodule Timesheet.Clock.Fake do
  @behaviour Timesheet.Clock

  def now do
    start
    Agent.get(__MODULE__, fn now -> now end)
  end

  def set_now(date) do
    start
    Agent.update(__MODULE__, fn _date -> date end)
  end

  defp start do
    Agent.start_link(fn -> :calendar.local_time end, name: __MODULE__)
  end
end
