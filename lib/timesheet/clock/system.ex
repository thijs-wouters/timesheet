defmodule Timesheet.Clock.System do
  @behaviour Timesheet.Clock

  def now do
    :calendar.local_time
  end
end
