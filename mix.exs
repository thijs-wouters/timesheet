defmodule Timesheet.Mixfile do
  use Mix.Project

  def project do
    [
      app: :timesheet,
      version: "0.0.10",
      elixir: "1.2.3",
      elixirc_paths: elixirc_paths(Mix.env),
      compilers: [:phoenix, :gettext] ++ Mix.compilers,
      build_embedded: Mix.env == :prod,
      start_permanent: Mix.env == :prod,
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
      ],
      deps: deps
    ]
  end

  # Configuration for the OTP application
  #
  # Type `mix help compile.app` for more information
  def application do
    [
      mod: {Timesheet, []},
      applications: [
        :phoenix,
        :phoenix_html,
        :cowboy,
        :logger,
        :phoenix_ecto,
        :postgrex,
        :calendar,
        :calendar_translations,
        :calecto,
        :connection,
        :gettext,
      ]
    ]
  end

  # Specifies which paths to compile per environment
  defp elixirc_paths(:test), do: ["lib", "web", "test/support"]
  defp elixirc_paths(_),     do: ["lib", "web"]

  # Specifies your project dependencies
  #
  # Type `mix help deps` for examples and options
  defp deps do
    [
      {:phoenix, "~> 1.1.0"},
      {:phoenix_ecto, "~> 2.0"},
      {:postgrex, ">= 0.0.0"},
      {:phoenix_html, "~> 2.3"},
      {:phoenix_live_reload, ">= 0.0.0", only: :dev},
      {:cowboy, ">= 0.0.0"},
      {:mix_test_watch, ">= 0.0.0", only: :dev},
      {:exrm, git: "https://github.com/bitwalker/exrm.git"},
      {:calendar, ">= 0.13.2"},
      {:calecto, ">= 0.0.0"},
      {:calendar_translations, "~> 0.0.3"},
      {:gettext, "~> 0.9"},
      {:excoveralls, "~> 0.4.5", only: :test},
      {:credo, "~> 0.2", only: [:dev, :test]},
    ]
  end
end
