defmodule Timesheet.Repo.Migrations.CreateEntry do
  use Ecto.Migration

  def change do
    create table(:entries) do
      add :date, :date
      add :time, :time
      add :type, :string

      timestamps
    end

  end
end
