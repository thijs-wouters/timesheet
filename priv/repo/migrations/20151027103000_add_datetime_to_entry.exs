defmodule Timesheet.Repo.Migrations.AddDatetimeToEntry do
  use Ecto.Migration

  def up do
    alter table(:entries) do
      add :datetime, :calendar_datetime
    end

    execute "update entries set datetime = (to_timestamp(date::text || ' ' || time::text || '.000000', 'YYYY-MM-DD HH24:MI:SS.US'), 3600, 'Europe/Brussels')"
  end

  def down do
    alter table(:entries) do
      remove :datetime
    end
  end
end
