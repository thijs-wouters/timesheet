defmodule Timesheet.Repo.Migrations.AddCommentToEntry do
  use Ecto.Migration

  def change do
    alter table(:entries) do
      add :comment, :string
    end
  end
end
