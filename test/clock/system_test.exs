defmodule Timesheet.Clock.SystemTest do
  use ExUnit.Case, async: true

  test "returns the system date and time" do
    assert {{_, _, _}, {_, _, _}} = Timesheet.Clock.System.now
  end
end
