defmodule Timesheet.EntryControllerTest do
  use Timesheet.ConnCase

  alias Timesheet.Entry
  alias Timesheet.Clock
  @valid_attrs %{
    datetime: %{
      "day" => "17", "month" => "4", "year" => "2010",
      "hour" => "14", "min" => "0"},
    type: "IN",
    comment: "",
  }
  @invalid_attrs %{}

  setup do
    conn = conn()
    {:ok, conn: conn}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, entry_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing entries"
  end

  test "all the entries are sorted by date", %{conn: conn} do
    expectedThird = an_entry |> on({{2015, 3, 2}, {14, 0, 0}}) |> create
    expectedSecond = an_entry |> on({{2015, 3, 2}, {15, 0, 0}}) |> create
    expectedFirst = an_entry |> on({{2015, 3, 3}, {14, 0, 0}}) |> create
    conn = get conn, entry_path(conn, :index)
    assert conn.assigns[:entries] == [expectedFirst, expectedSecond, expectedThird]
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, entry_path(conn, :new)
    assert html_response(conn, 200) =~ "New entry"
  end

  test "the default time is now for a new entry", %{conn: conn} do
    Clock.Fake.set_now({{2015, 1, 1}, {10, 0, 0}})
    conn = get conn, entry_path(conn, :new)
    assert Ecto.Changeset.get_field(conn.assigns[:changeset], :datetime) == DateTime.from_erl!({{2015, 1, 1}, {10, 0, 0}}, "Europe/Brussels", 0)
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, entry_path(conn, :create), entry: @valid_attrs
    assert redirected_to(conn) == entry_path(conn, :index)
    assert Repo.one(from(e in Entry, select: count(e.id))) == 1
  end

  test "saves the comment" do
    post conn, entry_path(conn, :create), entry: %{ @valid_attrs | comment: "testing" }
    assert Repo.one(Entry).comment == "testing"
  end

  test "sets the current datetime as a default", %{conn: conn} do
    Clock.Fake.set_now({{2015, 1, 1}, {12, 0, 0}})
    conn = post conn, entry_path(conn, :create), entry: %{ type: "IN" }
    assert redirected_to(conn) == entry_path(conn, :index)
    assert Repo.one(Entry).datetime == DateTime.from_erl!(
      {{2015, 1, 1}, {12, 0, 0}}, "Europe/Brussels", 0)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, entry_path(conn, :create), entry: @invalid_attrs
    assert html_response(conn, 200) =~ "New entry"
  end

  test "shows chosen resource", %{conn: conn} do
    entry = Repo.insert! %Entry{type: "IN"}
    conn = get conn, entry_path(conn, :show, entry)
    assert html_response(conn, 200) =~ "Show entry"
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_raise Ecto.NoResultsError, fn ->
      get conn, entry_path(conn, :show, -1)
    end
  end

  test "renders form for editing chosen resource", %{conn: conn} do
    entry = Repo.insert! %Entry{type: "IN"}
    conn = get conn, entry_path(conn, :edit, entry)
    assert html_response(conn, 200) =~ "Edit entry"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    {:ok, entry} = Repo.insert %Entry{type: "IN"}
    conn = put conn, entry_path(conn, :update, entry), entry: @valid_attrs
    assert redirected_to(conn) == entry_path(conn, :index)
    assert Repo.one(from(e in Entry, select: count(e.id))) == 1
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    entry = Repo.insert! %Entry{type: "IN"}
    conn = put conn, entry_path(conn, :update, entry), entry: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit entry"
  end

  test "deletes chosen resource", %{conn: conn} do
    entry = Repo.insert! %Entry{}
    conn = delete conn, entry_path(conn, :delete, entry)
    assert redirected_to(conn) == entry_path(conn, :index)
    refute Repo.get(Entry, entry.id)
  end
end
