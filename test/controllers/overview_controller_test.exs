defmodule Timesheet.OverviewControllerTest do
  use Timesheet.ConnCase

  alias Timesheet.EntryBuilder
  alias Timesheet.Clock
  alias Timesheet.Month

  setup do
    EntryBuilder.of_types([
      "IN",
      "OUT",
      "IN",
      "OUT",
      "IN",
      "OUT",
      "IN",
      "OUT",
      "IN",
      "OUT",
      "IN",
      "OUT",
      "IN",
      "OUT",
    ])
    |> EntryBuilder.entries_on([
      {{2016, 2, 29}, {10, 0, 0}},
      {{2016, 2, 29}, {12, 0, 0}},
      {{2016, 3, 1}, {10, 0, 0}},
      {{2016, 3, 1}, {12, 0, 0}},
      {{2016, 3, 3}, {10, 0, 0}},
      {{2016, 3, 3}, {12, 0, 0}},
      {{2016, 3, 31}, {10, 0, 0}},
      {{2016, 3, 31}, {12, 0, 0}},
      {{2016, 4, 1}, {10, 0, 0}},
      {{2016, 4, 1}, {12, 0, 0}},
      {{2016, 4, 30}, {10, 0, 0}},
      {{2016, 4, 30}, {12, 0, 0}},
      {{2016, 5, 1}, {10, 0, 0}},
      {{2016, 5, 1}, {12, 0, 0}},
    ])
    |> EntryBuilder.create

    {:ok, conn: conn()}
  end

  test "show complete overview on index", %{conn: conn} do
    conn = get conn, overview_path(conn, :index)
    assert html_response(conn, 200) =~ "Overview"
  end

  test "index shows the current month as a default", %{conn: conn} do
    Clock.Fake.set_now({{2016, 3, 10}, {10, 0, 0}})
    conn = get conn, overview_path(conn, :index)
    assert Enum.count(conn.assigns[:overview]) == 3
    assert conn.assigns[:chosen_month] == %Month{month: 3, year: 2016}
  end

  test "index with month as query param" do
    Clock.Fake.set_now({{2016, 3, 10}, {10, 0, 0}})
    conn = get conn, overview_path(conn, :index, month: "2016/4")
    assert Enum.count(conn.assigns[:overview]) == 2
    assert conn.assigns[:chosen_month] == %Month{month: 4, year: 2016}
  end
end
