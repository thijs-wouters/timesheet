defmodule Timesheet.EntryTest do
  use Timesheet.ModelCase
  use Calendar

  alias Timesheet.Entry

  @valid_attrs %{
    datetime: DateTime.now!("Europe/Brussels"),
    date: "2010-04-17",
    time: "14:00:00",
    comment: "",
    type: "IN"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Entry.changeset(%Entry{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Entry.changeset(%Entry{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "type can be IN" do
    changeset = Entry.changeset(%Entry{}, %{@valid_attrs | type: "IN"})
    assert changeset.valid?
  end

  test "type can be OUT" do
    changeset = Entry.changeset(%Entry{}, %{@valid_attrs | type: "OUT"})
    assert changeset.valid?
  end

  test "type cannot be anything else" do
    changeset = Entry.changeset(%Entry{}, %{@valid_attrs | type: "anything else"})
    refute changeset.valid?
  end

  test "an entry can have a comment" do
    changeset = Entry.changeset(%Entry{}, %{@valid_attrs | comment: "Stayed late"})
    assert changeset.valid?
  end

  test "the comment is not mandatory" do
    changeset = Entry.changeset(%Entry{}, %{@valid_attrs | comment: ""})
    assert changeset.valid?
  end
end
