defmodule Timesheet.OverviewTest do
  use Timesheet.ModelCase, async: true
  use Calendar

  import Timesheet.EntryBuilder

  alias Timesheet.Overview
  alias Timesheet.Duration

  doctest Overview

  setup do
    in_entry_1 = an_entry |> on({{2015, 3, 3}, {10, 0, 0}}) |> of_type("IN")
    out_entry_1 = an_entry |> on({{2015, 3, 3}, {12, 0, 0}}) |> of_type("OUT")
    in_entry_2 = an_entry |> on({{2015, 3, 4}, {10, 0, 0}}) |> of_type("IN")
    out_entry_2 = an_entry |> on({{2015, 3, 4}, {12, 10, 15}}) |> of_type("OUT")

    {
      :ok,
      no_work_logged: [
      ],
      not_working: [
        out_entry_2,
        in_entry_2,
        out_entry_1,
        in_entry_1,
      ],
      still_working: [
        in_entry_2,
        out_entry_1,
        in_entry_1,
      ],
    }
  end

  test "no work logged - no overview", %{
    no_work_logged: entries
  } do
    assert Overview.map(entries) == []
  end

  test "not working - time_in is the in entry", %{
    not_working: entries
  } do
    assert map_field(Overview.map(entries), :in) == [
      Enum.at(entries, 1),
      Enum.at(entries, 3),
    ]
  end

  test "not working - time_out is the out entry", %{
    not_working: entries
  } do
    assert map_field(Overview.map(entries), :out) == [
      Enum.at(entries, 0),
      Enum.at(entries, 2),
    ]
  end

  test "not working - hours_worked all calculated", %{
    not_working: entries
  } do
    assert map_field(Overview.map(entries), :hours_worked) == [
      %Duration{hours: 2, minutes: 10, seconds: 15},
      %Duration{hours: 2},
    ]
  end

  test "still working - time_in is the time of the in entry", %{
    still_working: entries
  } do
    assert map_field(Overview.map(entries), :in) == [
      Enum.at(entries, 0),
      Enum.at(entries, 2),
    ]
  end

  test "still working - first time_out is nil", %{
    still_working: entries
  } do
    assert map_field(Overview.map(entries), :out) == [
      nil,
      Enum.at(entries, 1),
    ]
  end

  test "still working - first duration is zero", %{
    still_working: entries
  } do
    assert map_field(Overview.map(entries), :hours_worked) == [
      %Duration{},
      %Duration{hours: 2},
    ]
  end

  defp map_field(structs, field) do
    Enum.map(structs, &Map.get(&1, field))
  end
end
