defmodule Timesheet.Plugs.TimezoneTest do
  use Timesheet.ConnCase

  @valid_attrs %{
    date: %{"day" => "17", "month" => "4", "year" => "2010"},
    time: %{"hour" => "14", "min" => "0"},
    datetime: %{"day" => "17", "month" => "4", "year" => "2010", "hour" => "14", "min" => "0"},
    type: "IN"}

  setup do
    {:ok, conn: conn()}
  end

  test "the given timezone is added to the conn" do
    conn = post conn, entry_path(conn, :create), entry: @valid_attrs
    assert conn.params
  end
end
