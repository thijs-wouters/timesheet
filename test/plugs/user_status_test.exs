defmodule Timesheet.Plugs.UserStatusTest do
  use Timesheet.ConnCase

  alias Timesheet.Plugs.UserStatus

  setup do
    {:ok, conn: conn()}
  end

  test "user_status is OUT when there are no entries", %{conn: conn} do
    conn = UserStatus.call(conn, [])
    assert conn.assigns[:user_status] == "OUT"
  end

  test "user_status is IN when the last entry is IN", %{conn: conn} do
    entries_on([
      {{2015, 1, 1}, {14, 0, 0}},
      {{2015, 1, 1}, {12, 0, 0}},
      {{2015, 1, 2}, {11, 0, 0}},
      {{2015, 1, 2}, {12, 0, 0}},
    ])
    |> of_types(["OUT", "OUT", "OUT", "IN"])
    |> create
    conn = UserStatus.call(conn, [])
    assert conn.assigns[:user_status] == "IN"
  end

  test "user_status is OUT when the last entry is OUT", %{conn: conn} do
    entries_on([
      {{2015, 1, 1}, {14, 0, 0}},
      {{2015, 1, 1}, {12, 0, 0}},
      {{2015, 1, 2}, {11, 0, 0}},
      {{2015, 1, 2}, {12, 0, 0}},
    ])
    |> of_types(["IN", "IN", "IN", "OUT"])
    |> create
    conn = UserStatus.call(conn, [])
    assert conn.assigns[:user_status] == "OUT"
  end
end
