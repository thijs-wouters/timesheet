defmodule Timesheet.DateBuilder do
  use Calendar

  def to_date(erl_date = {{_, _, _},{_, _, _}}) do
    case DateTime.from_erl(erl_date, "Europe/Brussels", 0) do
      {:ok, date} -> date
    end
  end
end
