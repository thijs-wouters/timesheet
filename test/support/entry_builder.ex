defmodule Timesheet.EntryBuilder do
  use Calendar

  import Timesheet.DateBuilder

  alias Timesheet.Entry

  def an_entry do
    %Entry{
      datetime: to_date(:calendar.local_time),
      type: "IN",
      comment: "",
    }
  end

  def entries_on(entries, datetimes) do
    entries
    |> Stream.zip(datetimes)
    |> Enum.map(fn({entry, datetime}) -> on(entry, datetime) end)
  end

  def entries_on(datetimes) do
    Stream.repeatedly(&an_entry/0)
    |> entries_on(datetimes)
  end

  def on(entry, datetime) do
    %Entry{entry | datetime: to_date(datetime)}
  end

  def of_types(entries, types) do
    entries
    |> Stream.zip(types)
    |> Enum.map(fn({entry, type}) -> of_type(entry, type) end)
  end

  def of_types(types) do
    Stream.repeatedly(&an_entry/0)
    |> of_types(types)
  end

  def of_type(entry, type) do
    %Entry{entry | type: type}
  end

  def create(entries) when is_list(entries) do
    Enum.map(entries, &create/1)
  end

  def create(entry) do
    Entry.changeset(entry, %{})
    |> Timesheet.Repo.insert
    |> elem(1)
  end
end
