defmodule Timesheet.LayoutViewTest do
  use Timesheet.ConnCase, async: true

  import Timesheet.LayoutView

  test "the inverse of IN is OUT" do
    assert inverse("IN") == "OUT"
  end

  test "the inverse of OUT is IN" do
    assert inverse("OUT") == "IN"
  end
end
