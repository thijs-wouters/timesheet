defmodule Timesheet.OverviewController do
  use Timesheet.Web, :controller

  alias Timesheet.Entry
  alias Timesheet.Repo
  alias Timesheet.Overview
  alias Timesheet.Month

  def index(conn, params) do
    chosen_month = Month.parse(params["month"]) || Month.current

    overview = Overview.map(Entry
                |> Entry.for_month(chosen_month)
                |> Entry.in_order
                |> Repo.all)

    render(conn, "index.html", overview: overview, chosen_month: chosen_month)
  end
end
