defimpl Phoenix.HTML.Safe, for: Timesheet.Duration do
  def to_iodata(%Timesheet.Duration{hours: hours, minutes: minutes}) do
    "#{hours} uur en #{minutes} minuten"
  end
end
