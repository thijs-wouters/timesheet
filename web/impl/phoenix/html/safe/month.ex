defimpl Phoenix.HTML.Safe, for: Timesheet.Month do
  use Calendar
  alias Timesheet.Month

  def to_iodata(month) do
    datetime = Month.start_of(month)
    case Strftime.strftime(datetime, "%B %Y", :nl) do
      {:ok, result} -> String.capitalize(result)
    end
  end
end
