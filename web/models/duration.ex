defmodule Timesheet.Duration do
  defstruct [hours: 0, minutes: 0, seconds: 0]

  alias __MODULE__

  use Calendar

  def between(start, finish) do
    case DateTime.diff(finish, start) do
      {:ok, seconds, _, _} -> Duration.from_seconds!(seconds)
    end
  end

  def from_seconds!(seconds) do
    %Duration{
      hours: to_hours(seconds),
      minutes: to_minutes(seconds),
      seconds: to_seconds(seconds),
    }
  end

  def none do
    %Duration{}
  end

  defp to_seconds(seconds), do: rem(seconds, 60)
  defp to_minutes(seconds), do: rem(div(seconds, 60), 60)
  defp to_hours(seconds), do: div(seconds, 60 * 60)
end
