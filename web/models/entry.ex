defmodule Timesheet.Entry do
  use Timesheet.Web, :model
  import Ecto.Query

  alias Timesheet.Month

  schema "entries" do
    field :datetime, Calecto.DateTime
    field :type, :string
    field :comment, :string

    timestamps
  end

  @required_fields ~w(datetime type)
  @optional_fields ~w(comment)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> validate_inclusion(:type, ["IN", "OUT"])
  end

  def in_order(query) do
    query
    |> order_by([e], desc: e.datetime)
  end

  def for_month(query, month) do
    start_of_month = Month.start_of(month)

    from entry in query,
    where: fragment("(?).wall_time >= ?", entry.datetime, type(^start_of_month, :datetime)),
    where: fragment("(?).wall_time < (? + interval '1 month')", entry.datetime, type(^start_of_month, :datetime))
  end
end
