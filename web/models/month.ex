defmodule Timesheet.Month do
  defstruct [:year, :month]

  alias __MODULE__
  alias Timesheet.Clock

  use Calendar

  def parse(nil), do: nil
  def parse(string) when is_binary(string) do
    Regex.named_captures(~r|(?<year>\d+)/(?<month>\d+)|, string)
    |> from
  end

  def from(%{"year" => year, "month" => month}) do
    from(%{year: year, month: month})
  end

  def from(hash = %{year: year}) when is_binary(year) do
    {year, ""} = Integer.parse(year)
    from(%{hash | year: year})
  end

  def from(hash = %{month: month}) when is_binary(month) do
    {month, ""} = Integer.parse(month)
    from(%{hash | month: month})
  end

  def from(%{year: year, month: month}) do
    %Month{year: year, month: month}
  end

  def start_of(%Month{year: year, month: month}) do
    {:ok, date} = DateTime.from_erl({{year, month, 1}, {0, 0, 0}}, "Europe/Brussels", 0)
    date
  end

  def current do
    from(Clock.now)
  end

  def previous(%Month{month: 1, year: year}) do
    %Month{year: year - 1, month: 12}
  end

  def previous(month) do
    %Month{month | month: month.month - 1}
  end

  def next(%Month{month: 12, year: year}) do
    %Month{year: year + 1, month: 1}
  end

  def next(month) do
    %Month{month | month: month.month + 1}
  end
end

defimpl String.Chars, for: Timesheet.Month do
  alias Timesheet.Month

  def to_string(%Month{month: month, year: year}) do
    "#{year}/#{month}"
  end
end
