defmodule Timesheet.Overview do
  alias __MODULE__
  alias Timesheet.Entry
  alias Timesheet.Duration

  defstruct [:in, :out, :hours_worked]

  def map([]), do: []
  def map([out_entry = %Entry{type: "OUT"} | rest]), do: map(out_entry, rest)
  def map([in_entry = %Entry{type: "IN"} | rest]) do
    [
      %Overview{
        in: in_entry,
        out: nil,
        hours_worked: Duration.none,
      }
      | map(rest)
    ]
  end

  defp map(out_entry = %Entry{type: "OUT"},
    [in_entry = %Entry{type: "IN"} | rest]) do

    [
      %Overview {
        in: in_entry,
        out: out_entry,
        hours_worked: Duration.between(in_entry.datetime, out_entry.datetime),
      }
      | map(rest)
    ]
  end
end
