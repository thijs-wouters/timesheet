defmodule Timesheet.Plugs.PageData do
  import Plug.Conn

  def init(opts) do
    opts
  end

  def call(conn = %Plug.Conn{private: %{phoenix_action: action}}, opts) do
    assign(conn, :page_data, Map.get(opts, action))
  end

  def page_data(conn) do
    {:safe, Poison.encode!(conn.assigns[conn.assigns[:page_data]])}
  end
end
