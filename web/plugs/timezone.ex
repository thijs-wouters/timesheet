defmodule Timesheet.Plugs.Timezone do
  def init(params), do: params

  def call(conn = %Plug.Conn{params: params, body_params: body_params}, _) do
    %Plug.Conn{conn |
      params: set_timezone(params),
      body_params: set_timezone(body_params)}
  end

  defp set_timezone(params = %{"entry" => entry}) do
    %{ params | "entry" => set_timezone(entry)}
  end
  defp set_timezone(map = %{"datetime" => datetime}) do
    %{ map | "datetime" => set_timezone(datetime) }
  end
  defp set_timezone(%{"day" => day, "month" => month, "year" => year,
    "min" => min, "hour" => hour }) do
    %{ "day" => day, "month" => month, "year" => year,
      "min" => min, "hour" => hour, "timezone" => "Europe/Brussels" }
  end
  defp set_timezone(map = %{}), do: map
end
