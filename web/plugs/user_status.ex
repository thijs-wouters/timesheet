defmodule Timesheet.Plugs.UserStatus do
  import Plug.Conn
  import Ecto.Query

  alias Timesheet.Entry
  alias Timesheet.Repo

  def init(params), do: params

  def call(conn, _) do
    Entry
    |> Entry.in_order
    |> limit([e], 1)
    |> Repo.all
    |> assign_user_status(conn)
  end

  defp assign_user_status([], conn), do: assign(conn, :user_status, "OUT")
  defp assign_user_status([%Timesheet.Entry{type: type}], conn) do
    assign(conn, :user_status, type)
  end
end
