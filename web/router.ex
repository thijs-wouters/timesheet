defmodule Timesheet.Router do
  use Timesheet.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug Timesheet.Plugs.UserStatus
    plug Timesheet.Plugs.Timezone
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Timesheet do
    pipe_through :browser # Use the default browser stack

    get "/", EntryController, :index
    resources "/entries", EntryController
    get "/overview", OverviewController, :index
  end

  # Other scopes may use custom stacks.
  # scope "/api", Timesheet do
  #   pipe_through :api
  # end
end
