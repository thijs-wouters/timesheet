defmodule Timesheet.EntryView do
  use Timesheet.Web, :view

  def pretty_print(datetime = %DateTime{}) do
    case Strftime.strftime(datetime, "%a %e %b %Y, %k:%M", :nl) do
      {:ok, result} -> result
    end
  end

  def active?(:entries), do: "active"
  def active?(_), do: ""
end
