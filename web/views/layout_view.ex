defmodule Timesheet.LayoutView do
  use Timesheet.Web, :view

  def inverse("OUT"), do: "IN"
  def inverse("IN"), do: "OUT"

  def active?(%Plug.Conn{private: %{phoenix_view: view}}, page) do
    view.active?(page)
  end
end
