defmodule Timesheet.OverviewView do
  use Timesheet.Web, :view

  alias Timesheet.Entry
  alias Timesheet.Month

  def pretty_print(nil), do: "-"
  def pretty_print(%Entry{datetime: time}), do: pretty_print(time)
  def pretty_print(datetime = %DateTime{}) do
    case Strftime.strftime(datetime, "%a %e %b %Y, %k:%M", :nl) do
      {:ok, result} -> result
    end
  end

  def active?(:overview), do: "active"
  def active?(_), do: ""

  def comment(nil), do: ""
  def comment(%Entry{comment: comment}), do: comment

  def previous_month(conn, month) do
    month_link(conn, Month.previous(month))
  end

  def next_month(conn, month) do
    month_link(conn, Month.next(month))
  end

  def month_link(conn, month) do
    link(Phoenix.HTML.Safe.to_iodata(month), to: overview_path(conn, :index, month: to_string(month)))
  end
end
